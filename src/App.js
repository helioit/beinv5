import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Header from './components/Header'
import Showcase from './components/Showcase'
import Destinations from './components/Destinations'
import Destinations2 from './components/Destinations2'
import Destinations3 from './components/Destinations3'
import Destinations5 from './components/Destinations5'
import Footer from './components/Footer'
import Login from './components/Login.js'
import SignUp from './components/SignUp'
import Error from './components/Error'

function App() {
  return (
    <Router>
      <Header />

      <Switch>
        <Route exact path='/'>
          <Showcase />
          <Destinations />
          <Destinations2/>
          <Destinations3/>
          <Destinations5/>
        </Route>
        <Route path='/destinations2'>
          <Destinations2 />
        </Route>
        <Route path='/destinations3'>
          <Destinations3 />
        </Route>
        <Route path='/destinations5'>
          <Destinations5 />
        </Route>
        <Route path='/login'>
          <Login />
        </Route>
        <Route path='/signup'>
          <SignUp />
        </Route>
        <Route path='*'>
          <Error />
        </Route>
      </Switch>
      <Footer />
    </Router>
  )
}

export default App
