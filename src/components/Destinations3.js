import React from 'react'
import image1 from '../assets/profile_male.jpg'
import image2 from '../assets/profile_male.jpg'
import image3 from '../assets/profile_male.jpg'

const Destinations3 = () => {
  return (
    <section className='destinations3'>
      <h3>The team behind Bein</h3>
      <h5>We are not some random startup guys though</h5>
      <div className='grid'>
        <div>
          <img src={image1} alt='destination-1' />
          <h3>Be01</h3>
          <p>
          Developers
          </p>
        </div>

        <div>
          <img src={image2} alt='destination-2' />
          <h3>Be02</h3>
          <p>
          Developers
          </p>
        </div>

        <div>
          <img src={image2} alt='destination-2' />
          <h3>Be03</h3>
          <p>
          Developers
          </p>
        </div>
   
      </div>
    </section>
  )
}

export default Destinations3
