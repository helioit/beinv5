import React from 'react'
import image1 from '../assets/A.Khoa.jpg'
import image2 from '../assets/A.Nhut.jpg'
import image3 from '../assets/image-3.png'

const Destinations2 = () => {
  return (
    <section className='destinations2'>
      <h3>The team behind Bein</h3>
      <h5>We are not some random startup guys though</h5>
      <div className='grid'>
        <div>
          <img src={image1} alt='destination-1' />
          <h3>Tran Dang Khoa (CEO)</h3>
          <p>
          Entrepreneur, Speaker, and Chairman of EVOL GROUP. 
          He has founded 6 successful businesses over the course of 14 years. 
          He is followed by 924,842 people on social media. 
          </p>
        </div>

   

        <div>
          <img src={image2} alt='destination-2' />
          <h3>Duong Hoang Nhut (CTO / Product Manager)</h3>
          <p>
          Co-founder, CTO of EVOL GROUP. He has a Master degree in Computer Science 
          and greatly contributes to the successful of EVOL by developing a powerful CRM system.
          </p>
        </div>
   
      </div>
    </section>
  )
}

export default Destinations2
