import React from 'react'
import image1 from '../assets/image-1.jpg'
import image2 from '../assets/image-2.jpg'
import image3 from '../assets/image-3.png'

const Destinations5 = () => {
  return (
    <section className='destinations5'>
      <h3>We need you</h3>
      <h5> This is a great opportunity for passionate people who are ambitious to build a tech product for million users.</h5>
      <div className='grid'>
        <div>
          {/* <img src={image1} alt='destination-1' /> */}
          <h3>UI/UX designer</h3>
          <p>
          As a UI/UX designer at Bein
          </p>
        </div>

        <div>
          {/* <img src={image2} alt='destination-2' /> */}
          <h3>QA/QC</h3>
          <p>
          As a QA/QC at Bein
          </p>
        </div>

        <div>
          {/* <img src={image2} alt='destination-2' /> */}
          <h3>JavaScript Developers</h3>
          <p>
          CWe are looking for JavaScript Developers from a wide range of levels 
          (junior/mid-level/senior) and roles (front end/mobile app/backend) to our team to speed up our project.
          </p>
        </div>
   
      </div>
    </section>
  )
}

export default Destinations5
