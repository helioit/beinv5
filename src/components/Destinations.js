import React from 'react'
import image1 from '../assets/A.Khoa2.jpg'
import image2 from '../assets/image-2.jpg'
import image3 from '../assets/image-3.png'

const Destinations = () => {
  return (
    <section className='destinations'>
      <div className='grid'>
        <div>
          <img src={image1} alt='destination-1' />
          <p>
          “The social media and advertising world are broken. We are furious at those giant social networks, just as you are. ”

<p>Tran Dang Khoa</p>
<p>CEO</p>
          </p>
        </div>

      </div>
    </section>
  )
}

export default Destinations
