import React from 'react'
import { Link } from 'react-router-dom'

const Showcase = () => {
  return (
    <section className='showcase'>
      <div className='showcase-overlay'>
        <h1 style={{ color: '#000000'}}>BeIn</h1>
        <h2 style={{ color: '#000000'}}>Everyone needs a place to be in</h2>
        <p style={{ color: 'black'}}>
        Bein is a social platform powered by our own native blockchain. A place for those who value meaningful engagement and build their own community. No forced ads, no data control, you are responsible for your digital footprints.
        </p>
        <nav className='navbar'>
        <ul>
          <Link className='links' to='/destinations5'>
            JOIN OUR TEAM
          </Link>
        </ul>
      </nav>
      </div>
    </section>
  )
}

export default Showcase
